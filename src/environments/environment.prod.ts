export const environment = {
  production: true,
  config: {
    assetPrefix: '/fabos/{{name}}/',
    queryHost: '',
    queryPort: '',
    queryPrefix: '/fabos/{{name}}/mes',
    svrHost: '',
    svrPort: '',
    svrPrefix: '/fabos/{{name}}/mes',
    authHost: '',
    authPort: '',
    authPrefix: '/fabos/{{name}}/mes',
    pencilHost: '',
    pencilPort: '',
    pencilPrefix: '/fabos/{{name}}/mes/pencil/pencil',
    monacoEditorPrefix: '/fabos/{{name}}/assets',
    debug: false
  }
};
