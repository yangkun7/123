import { DataSourceConfig } from 'app/services/data-source.service';

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  // config: {
  //   queryHost: '192.168.1.151',
  //   queryPort: '8889',
  //   queryPrefix: '',
  //   svrHost: '192.168.1.151',
  //   svrPort: '8890',
  //   svrPrefix: '',
  //   authHost: '192.168.1.151',
  //   authPort: '8891',
  //   authPrefix: '',
  //   debug: true
  // }
  config: {
    assetPrefix: '',
    queryHost: '',
    queryPort: '',
    queryPrefix: '/sa/fabos/{{name}}/mes',
    svrHost: '',
    svrPort: '',
    svrPrefix: '/sa/fabos/{{name}}/mes',
    authHost: '',
    authPort: '',
    authPrefix: '/sa/fabos/{{name}}/mes',
    pencilHost: '',
    pencilPort: '',
    pencilPrefix: '/sa/fabos/{{name}}/mes/pencil/pencil',
    monacoEditorPrefix: '',
    debug: true
  }
};
